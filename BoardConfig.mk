# A/B
AB_OTA_PARTITIONS += \
    vendor

# Dynamic
BOARD_QTI_DYNAMIC_PARTITIONS_PARTITION_LIST += vendor

# Vendor
BOARD_PREBUILT_VENDORIMAGE := vendor/xiaomi/spes-prebuilt/vendor.img
